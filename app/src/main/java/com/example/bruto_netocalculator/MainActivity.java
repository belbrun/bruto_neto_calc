package com.example.bruto_netocalculator;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    EditText money;
    EditText children;
    EditText persons;
    EditText stopa;
    CheckBox pillar;
    Button calculate;
    boolean neto = false;
    boolean[] disability = new boolean[]{true,false,false};



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        money = findViewById(R.id.money);
        children= findViewById(R.id.children);
        persons = findViewById(R.id.persons);
        stopa= findViewById(R.id.stopa);
        pillar= findViewById(R.id.pillar);

        calculate = findViewById(R.id.calculate);
        calculate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calculate();
            }
        });
    }


    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.neto_button:
                if (checked){
                    neto = true;
                }
                break;

            case R.id.bruto_button:
                if (checked){
                    neto = false;
                }
                break;

            case R.id.no:
                if(checked) {
                    handleButtons(0);
                }
                break;

            case R.id.partial:
                if(checked) {
                    handleButtons(1);
                }
                break;

            case R.id.full:
                if(checked) {
                    handleButtons(2);
                }
                break;

        }


    }

    private void handleButtons(int index){
        for (int i = 0; i<3; i++){
            if(i == index){
                disability[i] = true;
            } else  {
                disability[i] = false;
            }
        }
    }
    public void calculate() {
        Locale locale = new Locale("en", "UK");
        DecimalFormatSymbols symbols = new DecimalFormatSymbols(locale);
        symbols.setDecimalSeparator('.');
        DecimalFormat df = new DecimalFormat("###.##", symbols);

        Double temp, temp2, temp3, temp4, temp5;

        double[] results = new double[25];
        if (neto) {
            if(money.getText().length() < 1){
                money.setText("0");
            }
            if(children.getText().length() < 1){
                children.setText("0");
            }
            if(persons.getText().length() < 1){
                persons.setText("0");
            }
            if(stopa.getText().length() < 1) {
                stopa.setText("0");
            }
            temp = Double.parseDouble(money.getText().toString());
            results[11] =temp;
             results[18] = temp;

            if (pillar.isChecked()) {
                 results[15] = 15 ;
                 results[16] = 5 ;
            } else {
                 results[15] = 20 ;
                 results[16] = 0 ;
            }
            temp = Double.parseDouble(stopa.getText().toString());
             results[17] = temp;

            temp2 = Double.parseDouble(children.getText().toString());
            temp = 3800.00;
            if (temp2 == 0) {
                temp = 3800.00;
            } else if (temp2 == 1) {
                temp = temp + 1750;
            } else if (temp2 == 2) {
                temp = temp + 4250;
            } else if (temp2 == 3) {
                temp = temp + 7750;
            } else if (temp2 == 4) {
                temp = temp + 12500;
            } else if (temp2 == 5) {
                temp = temp + 18750;
            } else if (temp2 == 6) {
                temp = temp + 26750;
            } else if (temp2 == 7) {
                temp = temp + 36750;
            } else if (temp2 == 8) {
                temp = temp + 49000;
            } else if (temp2 == 9) {
                temp = temp + 63750;
            }

            temp2 = Double.parseDouble(persons.getText().toString());
            for (int i = 0; i < temp2; i++) {
                temp = temp + 1750;
            }

            if (disability[1]) {
                temp = temp + 1000;
            }
            else if (disability[2]) {
                temp = temp + 3750;
            }

            results[8] = temp;

            temp2 = temp;
            temp = Double.parseDouble(money.getText().toString());

            if(temp <= temp2){
                temp3 = 100 * temp;
                temp3 = temp3 / (100 - 20);
                results[4] = temp3;
                 results[19] = temp3;
                results[7] =temp;

                results[9] = 0;
                results[10] = 0;
                results[12] = 0;
                results[13] = 0;
                results[14] = 0;

                if (pillar.isChecked()) {
                     results[15] = 15 ;
                     results[16] = 5 ;
                    temp = results[4];
                    temp = temp * 0.15;
                    results[5] =temp;
                    temp = results[4];
                    temp = temp * 0.05;
                    results[6] = temp;
                } else {
                     results[15] = 20 ;
                     results[16] = 0 ;
                    temp = results[4];
                    temp = temp * 0.20;
                    results[5] =temp;
                    results[6] =0;
                }
                temp = results[4];
                temp = temp * 0.15;
                temp2 = temp;
                results[1] = temp;
                temp = results[4];
                temp = temp * 0.005;
                temp2 = temp2 + temp;
                results[2] =temp;
                temp = results[4];
                temp = temp * 0.017;
                temp2 = temp2 + temp;
                results[3] =temp;
                temp = results[4];
                temp2 = temp2 + temp;
                results[0] = temp2;
            }
            else{
                temp = results[11] ;
                temp2 = results[8];
                temp3 = Double.parseDouble(stopa.getText().toString());
                temp4 = temp2 + 17500 - 175 * 24 - 1.75 * 24 * temp3;

                if(temp <= temp4){
                    temp5 = (temp - temp2 * (0.24 + 0.0024 * temp3)) / (0.608 - 0.00192 * temp3);

                    results[4] = temp5;
                     results[19] = temp5;


                    temp = results[4];
                    temp = temp * 0.15;
                    temp2 = temp;
                    results[1] = temp;
                    temp = results[4];
                    temp = temp * 0.005;
                    temp2 = temp2 + temp;
                    results[2] =temp;
                    temp = results[4];
                    temp = temp * 0.017;
                    temp2 = temp2 + temp;
                    results[3] =temp;
                    temp = results[4];
                    temp2 = temp2 + temp;
                    results[0] = temp2;

                    temp = results[4];
                    temp = temp * 0.8;
                    results[7] =temp;

                    if (pillar.isChecked()) {
                         results[15] = 15 ;
                         results[16] = 5 ;
                        temp = results[4];
                        temp = temp * 0.15;
                        results[5] =temp;
                        temp = results[4];
                        temp = temp * 0.05;
                        results[6] = temp;
                    } else {
                         results[15] = 20 ;
                         results[16] = 0 ;
                        temp = results[4];
                        temp = temp * 0.20;
                        results[5] =temp;
                        results[6] =0;
                    }
                    temp = results[7];
                    temp2 = results[8];

                    temp3 = temp - temp2;
                    temp4 = temp3;
                    if(temp4 <= 17500){
                        temp3 = temp3 * 0.24;
                        results[12] = temp3;
                        results[13] = 0;
                        results[10] = temp3;
                        temp = Double.parseDouble(stopa.getText().toString());
                         results[17] = temp;
                        temp = temp3 * (temp * 0.01);
                        results[14] =temp;
                        temp = temp + temp3;
                        results[9] =temp;
                    }
                }
                temp = results[11] ;
                temp2 = results[8];
                temp3 = Double.parseDouble(stopa.getText().toString());
                temp4 = temp2 + 17500 - 175 * 24 - 1.75 * 24 * temp3;

                if(temp > temp4){
                    temp4 = (temp2 * (36 + 0.36 * temp3) - 11272.99 * temp3 + 2587420.80);
                    temp4 = temp4 / 100;

                    if(temp <= temp4){
                        temp4 = temp - temp2 * (0.36 + 0.0036 * temp3) - 21 * temp3 - 2100;
                        temp4 = temp4 / (0.512 - 0.00288 * temp3);

                        results[4] = temp4;
                         results[19] = temp4;

                        temp = results[4];
                        temp = temp * 0.15;
                        temp2 = temp;
                        results[1] = temp;
                        temp = results[4];
                        temp = temp * 0.005;
                        temp2 = temp2 + temp;
                        results[2] =temp;
                        temp = results[4];
                        temp = temp * 0.017;
                        temp2 = temp2 + temp;
                        results[3] =temp;
                        temp = results[4];
                        temp2 = temp2 + temp;
                        results[0] = temp2;

                        temp = results[4];
                        temp = temp * 0.8;
                        results[7] =temp;

                        if (pillar.isChecked()) {
                             results[15] = 15 ;
                             results[16] = 5 ;
                            temp = results[4];
                            temp = temp * 0.15;
                            results[5] =temp;
                            temp = results[4];
                            temp = temp * 0.05;
                            results[6] = temp;
                        } else {
                             results[15] = 20 ;
                             results[16] = 0 ;
                            temp = results[4];
                            temp = temp * 0.20;
                            results[5] =temp;
                            results[6] =0;
                        }

                        temp = results[7];
                        temp2 = results[8];
                        temp3 = temp - temp2;

                        temp4 = 17500 * 0.24;
                        results[12] = temp4;
                        temp5 = (temp3 - 17500) * 0.36;
                        results[13] = temp5;
                        temp4 = temp4 + temp5;
                        results[10] = temp4;
                        temp = Double.parseDouble(stopa.getText().toString());
                         results[17] = temp;
                        temp = temp4 * (temp * 0.01);
                        results[14] =temp;
                        temp = temp + temp4;
                        results[9] =temp;
                    }
                }
            }
        }
////////////////////////////////////////////////////////////////////////////////////////////////////
        else {
            if(money.getText().length() < 1){
                money.setText("0");
            }
            if(children.getText().length() < 1){
                children.setText("0");
            }
            if(persons.getText().length() < 1){
                persons.setText("0");
            }
            if(stopa.getText().length() < 1) {
                stopa.setText("0");
            }

            temp = Double.parseDouble(money.getText().toString());
            results[4] =temp;
             results[19] = temp;

            if (pillar.isChecked()) {
                 results[15] = 15 ;
                 results[16] = 5 ;
                temp = Double.parseDouble(money.getText().toString());
                temp = temp * 0.15;
                results[5] =temp;
                temp = Double.parseDouble(money.getText().toString());
                temp = temp * 0.05;
                results[6] = temp;
            } else {
                 results[15] = 20 ;
                 results[16] = 0 ;
                temp = Double.parseDouble(money.getText().toString());
                temp = temp * 0.20;
                results[5] =temp;
                results[6] =0;
            }
            temp = Double.parseDouble(money.getText().toString());
            temp = temp * 0.15;
            temp2 = temp;
            results[1] = temp;
            temp = Double.parseDouble(money.getText().toString());
            temp = temp * 0.005;
            temp2 = temp2 + temp;
            results[2] =temp;
            temp = Double.parseDouble(money.getText().toString());
            temp = temp * 0.017;
            temp2 = temp2 + temp;
            results[3] =temp;
            temp = results[4];
            temp2 = temp2 + temp;
            results[0] = temp2;

            temp = Double.parseDouble(money.getText().toString());
            temp = temp * 0.8;
            results[7] =temp;

            temp2 = Double.parseDouble(children.getText().toString());
            temp = 3800.00;
            if(temp2 == 0){
                temp = 3800.00;
            }else if(temp2 == 1){
                temp = temp + 1750;
            }else if(temp2 == 2){
                temp = temp + 4250;
            }else if(temp2 == 3){
                temp = temp + 7750;
            }else if(temp2 == 4){
                temp = temp + 12500;
            }else if(temp2 == 5){
                temp = temp + 18750;
            }else if(temp2 == 6){
                temp = temp + 26750;
            }else if(temp2 == 7){
                temp = temp + 36750;
            }else if(temp2 == 8){
                temp = temp + 49000;
            }else if(temp2 == 9){
                temp = temp + 63750;
            }

            temp2 = Double.parseDouble(persons.getText().toString());
            for(int i = 0; i < temp2; i++){
                temp = temp + 1750;
            }

            if(disability[1]){
                temp = temp + 1000;
            }
            if(disability[2]){
                temp = temp + 3750;
            }

            results[8] = temp;

            temp = results[7];
            temp2 = results[8];

            temp4 = results[5];
            temp5 = results[6];
            temp3 = Double.parseDouble(money.getText().toString());
            temp3 = temp3 - temp4;
            temp3 = temp3 - temp5;
            results[11] = temp3;
             results[18] = temp3;

            if(temp < temp2){
                results[9] = 0;
                results[10] = 0;
                results[12] = 0;
                results[13] = 0;
                results[14] = 0;
            }
            else{
                temp3 = temp - temp2;
                temp4 = temp3;
                if(temp4 <= 17500){
                    temp3 = temp3 * 0.24;
                    results[12] = temp3;
                    results[13] = 0;
                    results[10] = temp3;
                    temp = Double.parseDouble(stopa.getText().toString());
                     results[17] = temp;
                    temp = temp3 * (temp * 0.01);
                    results[14] =temp;
                    temp = temp + temp3;
                    results[9] =temp;

                    temp4 = results[5];
                    temp5 = results[6];
                    temp3 = Double.parseDouble(money.getText().toString());
                    temp3 = temp3 - temp4;
                    temp3 = temp3 - temp5;
                    temp3 = temp3 - temp;
                    results[11] = temp3;
                     results[18] = temp3;
                }
                else{
                    temp4 = 17500 * 0.24;
                    results[12] = temp4;
                    temp5 = (temp3 - 17500) * 0.36;
                    results[13] = temp5;
                    temp4 = temp4 + temp5;
                    results[10] = temp4;
                    temp = Double.parseDouble(stopa.getText().toString());
                     results[17] = temp;
                    temp = temp4 * (temp * 0.01);
                    results[14] =temp;
                    temp = temp + temp4;
                    results[9] =temp;

                    temp4 = results[5];
                    temp5 = results[6];
                    temp3 = Double.parseDouble(money.getText().toString());
                    temp3 = temp3 - temp4;
                    temp3 = temp3 - temp5;
                    temp3 = temp3 - temp;
                    results[11] = temp3;
                     results[18] = temp3;
                }
            }
        }

        Intent i = new Intent(this, ResultsActivity.class);
        i.putExtra("RESULTS", results);
        startActivity(i);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (money.getText().toString().equals("0")) money.setText("");
        if (children.getText().toString().equals("0")) children.setText("");
        if (children.getText().toString().equals("0")) persons.setText("");
        if (children.getText().toString().equals("0")) stopa.setText("");
    }
}
