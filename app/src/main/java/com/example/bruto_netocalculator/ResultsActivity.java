package com.example.bruto_netocalculator;

import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class ResultsActivity extends AppCompatActivity {

    double numbers[];
    Button close;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_results);

        Bundle bundle = getIntent().getExtras();

        numbers = bundle.getDoubleArray("RESULTS");

        initializeTextViews();

        close = findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                close();
            }
        });
    }

    private void close(){

        finish();
    }

    private void initializeTextViews(){
        ((TextView)findViewById(R.id.jedan)).setText(String.format("Ukupan trošak: %.2f", numbers[0]));
        ((TextView)findViewById(R.id.jedan)).setTypeface(Typeface.DEFAULT_BOLD);
        ((TextView)findViewById(R.id.dva)).setText(String.format("Zdravstveno osiguranje (15%%) : %.2f", numbers[1]));
        ((TextView)findViewById(R.id.tri)).setText(String.format("Zaštita zdr. na radu (0.5%%) : %.2f", numbers[2]));
        ((TextView)findViewById(R.id.cetri)).setText(String.format("Zapošljavanje (1.7%%) : %.2f", numbers[3]));
        ((TextView)findViewById(R.id.pet)).setText(String.format("Bruto plaća: %.2f", numbers[4]));
        ((TextView)findViewById(R.id.pet)).setTypeface(Typeface.DEFAULT_BOLD);
        ((TextView)findViewById(R.id.sest)).setText(String.format("Doprinos za mirovinsko 1. stup : %.2f", numbers[5]));
        ((TextView)findViewById(R.id.seam)).setText(String.format("Doprinos za mirovinsko 2. stup : %.2f", numbers[6]));
        ((TextView)findViewById(R.id.osam)).setText(String.format("Dohodak : %.2f", numbers[7]));
        ((TextView)findViewById(R.id.osam)).setTypeface(Typeface.DEFAULT_BOLD);
        ((TextView)findViewById(R.id.devet)).setText(String.format("Osobni dobitak : %.2f", numbers[8]));
        ((TextView)findViewById(R.id.devet)).setTypeface(Typeface.DEFAULT_BOLD);
        ((TextView)findViewById(R.id.deset)).setText(String.format("Porez i prirez ukupno : %.2f", numbers[9]));
        ((TextView)findViewById(R.id.deset)).setTypeface(Typeface.DEFAULT_BOLD);
        ((TextView)findViewById(R.id.jedana)).setText(String.format("Ukupno porez : %.2f", numbers[10]));
        ((TextView)findViewById(R.id.dvana)).setText(String.format("Porez (24%%) : %.2f", numbers[12]));
        ((TextView)findViewById(R.id.trina)).setText(String.format("Porez (36%%) : %.2f", numbers[13]));
        ((TextView)findViewById(R.id.cetrina)).setText(String.format("Neto plaća : %.2f", numbers[11]));
        ((TextView)findViewById(R.id.cetrina)).setTypeface(Typeface.DEFAULT_BOLD);
        ((TextView)findViewById(R.id.petrina)).setText(String.format("Prirez : %.2f", numbers[14]));
        ((TextView)findViewById(R.id.sestrina)).setText(String.format("Bruto plaća : %.2f", numbers[19]));
    }
}
